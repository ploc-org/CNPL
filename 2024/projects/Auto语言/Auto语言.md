
1. 项目名称：Auto语言

2. 项目图标：

![auto_logo](./auto_logo.png)

3. 项目主页：[https://gitee.com/auto-stack/auto-lang](https://gitee.com/auto-stack/auto-lang)

4. 项目仓库：[https://gitee.com/auto-stack/auto-lang](https://gitee.com/auto-stack/auto-lang)

5. 项目简介。

Auto语言是一门适用于多场景的编程语言。基于Rust实现。

Auto语言的特色有：

- 灵活多变。适应多种生态（C、Rust、Python、Shell等）。针对不同场景，Auto语言提供不同的语法、语言特性和标准库。
- 动静皆宜。既支持动态类型，也支持静态类型。既能动态解释，也能静态编译执行。
- 全栈俱备。Auto语言有自己的标准库、REPL、构建器和UI框架。Auto语言支持前端UI、后端服务和嵌入式开发。

Auto语言可以用于下列场景：

a. 作为UI界面的描述语言。

![auto_ui](./auto_ui.png)

这个界面的Auto代码如下：

```typescript
// 数据：省略
var cols = [...]
var service_data = [...]
var item_list = [...]

// 定义一个UI组件
widget service_table {
    model {
        var config = cols
        var data = service_data
    }

    view {
        table(config, data)
    }
}

// APP的UI布局描述
app {
    left {
        list(item_list)
    }
    center {
        tabs {
            tab("service") {
                service_table()
            }
            tab("signals") {
                text("signals")
            }
            tab("messages") {
                text("messages")
            }
        }
    }
    right {
        text("right pane")
    }
    bottom {
        text("bottom pane")
    }
}
```

AutoUI是Auto语言的UI框架，底层基于Rust/GPUI库实现。
具体可以参考[AutoUI的工程主页](https://gitee.com/auto-stack/auto-ui)。

在这个场景中，Auto语言采取的形式是“UI声明”（AutoSpec），语法类似于Jetpack Compose。


b. 作为配置语言，管理Auto/C语言的混合工程。

```typescript
project: "hello"
version: "0.1.0"

// 依赖库自动下载
dep(log, "0.1.0")

// 本工程中的lib库
lib("mymath") {
    link: log
}

// 本工程中的exe程序
exe("hello") {
    link: mymath
}
```

AutoMan是Auto语言的工程管理器，它支持：

- Auto语言的编译
- C语言的模块化支持
- 依赖包的管理和下载
- 支持多种编译器（GCC、MSVC、IAR、GHS等）
- 可以输出成多种IDE的工程（CMake、Visual Studio、IAR EWARM、Eclipse CDT等）。

在这个场景中，Auto语言的形式是`AutoConfig`，语法类似于加强版的JSON。


c. 作为C语言的替代，嵌入到C工程中。

Auto语言可以翻译成C语言，并使用`AutoMan`工具管理Auto/C的混合工程。

```rust
// math.at
pub fn add(a int, b int) int {
    a + b
}
```

可以生成C文件：math.h和math.c

```c
// math.h
#ifndef _MATH_H_
#define _MATH_H_
#include <stdint.h>

int32_t add(int32_t a, int32_t b);
#endif
```

```c
// math.c
#include <stdint.h>
#include "math.h"

int32_t add(int32_t a, int32_t b) {
    return a + b;
}
```

d. 作为Shell脚本，实现跨平台的脚本功能。

例如，如下脚本：

```bash
#!auto

cd ~/logs/20241120
grep "error" *.log | wc -l
```

会被解析为如下Auto代码：

```rust
use std::shell::*

fn main {
    cd("~/logs/20241120")
    let output1 = grep("error", "*.log").output
    wc(output1, l=true)
}
```

`AutoShell`是Auto语言的shell库，它实现了常用的shell命令，并支持管道操作。

e. 作为模版，用于C/Rust/HTML等代码的生成。

例如：

```javascript
<table>
$ for i, s in students {
    <tr>
        <td>$i: </td>
        <td>${s.name}</td>
        <td>${s.age}</td>
    </tr>
$ }
</table>
```

Auto语言会自动展开循环，填入`students`中的数据，生成完整的HTML代码。

在这个场景中，Auto语言的形式是`AutoTemplate`，语法类似于Python的Jinja模版。

f. 作为嵌入式脚本，辅助Rust的开发。

例如，在规划中的引擎项目`AutoEngine`中，
底层的3D图形引擎使用Rust的bevy引擎，
而Auto语言则作为脚本语言来管理游戏逻辑。

6. 项目分类：  
免费、开源（MIT）、通用、接受社区贡献  
语言类别：面向场景语言；动态语言；静态语言。
工具类别：解释器、转译器（C、Rust、Python）、REPL、Builder。
应用领域：汽车行业、嵌入式开发、机器人、UI应用开发、3D图形应用。


7. 联系方式：visus@qq.com（电子邮箱）
