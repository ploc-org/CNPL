1. 项目名称：MoonBit

2. 项目图标：

![MoonBit Logo](./logo.png)

3. 项目主页：[https://www.moonbitlang.cn](https://www.moonbitlang.cn)

4. 项目仓库：[https://github.com/moonbitlang/core](https://github.com/moonbitlang/core)

5. 项目简介：

MoonBit 是一个语法与 Rust 类似的编程语言（带有 GC 支持），带有现代化的工具链及多后端支持。

```moonbit
fn main {
  println("Hello World!")
}
```

主要优势：

- 优秀的编译与构建速度，
- 简单但实用、面向数据的语言设计，
- 多后端支持，包含 WebAssembly、WebAssembly 使用 GC 提案、JavaScript、C 后端等。
- 生成 WebAssembly 代码体积小，运行速度快；生成 C 后端运行高效。

特性：

- 云原生开发：MoonBit 提供云原生 IDE，包含所有现代 IDE 的功能，可以在不依赖任何后端的情况下在浏览器中进行项目开发、运行、测试、调试等；
- 全链条设计：MoonBit 设计时便规划实现完整生态链，包括IDE、编译器、语言服务器等组件，及开发、测试、调试、发布等流程；
- 面向AI设计：MoonBit 设计时便考虑与AI的结合，语言设计便于AI生成。所有函数、方法均在顶层定义，具有完整类型声明，并且采用结构化Trait，如：

  ```moonbit
  pub(open) trait Animal {
    speak(Self) -> Unit
  }
  
  struct Dog { }
  
  // 实现 Animal
  pub fn speak(self : Dog) -> Unit {
    println("Bark")
  }
  
  let animals : Array[Animal] = [ Dog::{} ]
  ```

- 面向应用开发者设计：MoonBit 原生支持 JSON 语法，如：

  ```moonbit
  let data : Json = {
      "object" : { "key": 1, "value" : "v" },
      "array" : [ 1, true ]
  }
  ```

MoonBit is a programming language with a syntax similar to Rust, having GC and coming with modern toolchains and multi-backends.

```moonbit
fn main {
  println("Hello World!")
}
```

Main advantages：

- Excellent compilation and build speed,
- Simple and practical data-oriented language design,
- Multi-backends: including WebAssembly, WebAssembly with GC, JavaScript and C,
- Generated WebAssembly is small in size and fast in execution; generated C code is efficient.

Features:

- Cloud-native development: MoonBit comes with a cloud-native IDE, which has features for modern IDEs and enables developing, executing, testing and debugging in browser without any backends.
- Overall design: MoonBit is designed with the whole toolchain in mind, including such components as IDE, compiler, language server, and such process as developing, testing, debugging and publishing.
- AI-oriented design: MoonBit is designed to cooperate better with AI, having a language design that allows better code generation. All the functions, methods are defined at top-level with complete type declaration and structural traits are used, such as:

  ```moonbit
  pub(open) trait Animal {
    speak(Self) -> Unit
  }
  
  struct Dog { }
  
  // implements Animal
  pub fn speak(self : Dog) -> Unit {
    println("Bark")
  }
  
  let animals : Array[Animal] = [ Dog::{} ]
  ```

- Developer-oriented design: MoonBit support JSON grammar, like this example:

  ```moonbit
  let data : Json = {
      "object" : { "key": 1, "value" : "v" },
      "array" : [ 1, true ]
  }
  ```

项目展示：

- 使用 MoonBit 与 Wasm4 开发游戏，运行在浏览器与 ESP-C6 单片机中
  ![MoonBit编写的游戏运行于浏览器与单片机中](./wasm4.png)
- 使用 MoonBit 开发的网页应用
  ![MoonBit编写的网页应用](./ui.png)

Project Demo:
- Games developed using MoonBit and Wasm4, running in browser and on ESP-C6 micro-controller.
  ![Game written in MoonBit running in browser and on ESP-C6 micro-controller](./wasm4.png)
- Web App developed using MoonBit
  ![Web App written in MoonBit](./ui.png)


6. 项目分类：免费、开源、通用、接受社区贡献
语言类别：通用编程语言
工具类别：一般编译工具
应用领域：通用

7. 联系方式：
[jichuruanjian@idea.edu.cn](mailto:jichuruanjian@idea.edu.cn)