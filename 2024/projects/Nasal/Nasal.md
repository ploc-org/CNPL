
1. 项目名称：Nasal-Interpreter

2. 项目图标

![logo](./nasal_transparent.svg)

3. 项目主页：[https://www.fgprc.org.cn/nasal_interpreter.html](https://www.fgprc.org.cn/nasal_interpreter.html)

4. 项目仓库：[https://github.com/ValKmjolnir/Nasal-Interpreter](https://github.com/ValKmjolnir/Nasal-Interpreter)

5. 项目简介：

```javascript
print("hello world!");
```

Nasal 是一款语法与 ECMAscript 相似的脚本语言，设计者为 Andy Ross。后来被引入著名的开源飞行模拟器 FlightGear 中，作为 FlightGear 机模开发专用的脚本语言。

由于在 FlightGear 中，使用内嵌的 Nasal 控制台窗口进行调试很不方便，仅仅是想检查语法错误，也得花大量时间打开软件等待加载后进行调试。
所以一个全新的 Nasal 解释器诞生了。项目的初衷是帮助开发者检查语法错误，甚至是运行时的错误。在近期的迭代中，Nasal-Interpreter 还支持了 REPL 解释器，跨平台 subprocess，以及更加详细的错误 trace back 信息。

Nasal 的基础数据类型也非常简单，复杂的数据类型可以通过基础类型的组合来实现:

```javascript
var this_is_number = 0.0;
var this_is_string = "i am string";
var this_is_vector = [0, "i am vector", ["another vector"]];
var this_is_hash = {
    field_name: "field_value",
    parents: [{}]
};
```

Nasal 的函数实际上是 Lambda，可以作为数据进行传递:

```javascript
var this_is_function = func(a, b) {
    return a + b;
}

var hash = {
    f: this_is_function,
    example: func(a, b) {
        # `me` acts like `this` in other languages
        return me.f(a, b);
    }
};

print(hash.f(1, 2), "\n"); # expect 3
print(hash.example(2, 4), "\n"); # expect 6
```

Nasal 使用一个比较特别的机制来模拟继承:

```javascript
var parent = {
    prt: func { print("in parent function\n"); }
}

var child = {
    parents: [parent]
}

child.prt(); # expect "in parent function\n"
```

Nasal 还使用两种特别的循环语法来方便脚本的编写：

```javascript
forindex(var i; [0, 0, 0]) {
    print(i); # expect 012
}
foreach(var i; ['foo', 'bar']) {
    print(i, " "); # expect foo bar
}
```

6. 项目分类：免费、开源（GPLv2）、通用、接受社区贡献  
语言类别：一般编程语言、命令式语言、解释型脚本语言  
工具类别：脚本语言、解释器  
应用领域：通用、行业应用  

7. 联系方式：  
lhk101lhk101@qq.com（电子邮箱）  
sidi.liang@gmail.com（电子邮箱）
