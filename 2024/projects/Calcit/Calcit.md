1. 项目名称：Calcit

2. 项目图标：

![](./logo.png)

3. 项目主页：[https://calcit-lang.org/](https://calcit-lang.org/)

4. 项目仓库：[https://github.com/calcit-lang/calcit](https://github.com/calcit-lang/calcit)

5. 项目简介。

Calcit 是 Clojure 的方言, 遵循不可变数据结构、前缀表达式、Macros 作为核心设计. 使用 Rust 实现, 能够快速启动和运行. Calcit 可以直接解释执行, 也可以编译为 JavaScript 代码再执行. 除了通过 JavaScript 生态的工具链做代码热替换, Calcit 解释器自身也实现了代码热替换功能, 借助增量信息增量更新程序代码, 而无需重新启动进程.

生成代码时配合 ES Modules 等现代前端开发习惯进行了简化, 相比 ClojureScript 方案更轻量, 更易于同 JavaScript 代码混用, 也一定程度降低调试成本.

Calcit 的文本形态使用缩进语法.

代码示例一, 简单的数据变换, 类似 Clojure 语法中的 threading macros:

```cirru
->
  range 100
  map $ fn (x)
    * x x
  foldl 0 &+
  println
```

代码示例二, 使用 Macro 封装的基于 Calcit 生态定义的前端 Virtual DOM 组件写法:

```cirru
defcomp comp-inspect (tip data style)
  let
      class-name $ if (string? style) style
      style-map $ if (map? style) style
    pre $ {}
      :class-name $ str-spaced style-data class-name
      :inner-text $ str tip "|: " (grab-info data)
      :style style-map
      :on-click $ fn (e d!)
        if (some? js/window.devtoolsFormatters) (js/console.log data)
          js/console.log $ to-js-data data
```

实际开发中 Calcit 使用数据文件来存储源码. 支持使用结构化的方式直接以表达式为单元进行编辑, 编辑器内部以数据形态展开, 因而也能快速完成部分定义调整和代码重整, 从而提升动态类型语言的编写和修改速度:

![](./editor.webp)

Calcit 主要应用于 Web 页面开发场景. 实现了部分 Virtual DOM 生态的功能.

6. 项目分类：免费、开源（MIT）、通用、接受社区贡献(倾向于类库方式贡献)
   语言类别：一般编程语言、脚本语言
   工具类别：JavaScript 生成工具, 解释器
   应用领域：Web 开发

7. 联系方式：jiyiinyiyong@gmail.com（电子邮箱）
