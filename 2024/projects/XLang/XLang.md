1. 项目名称：XLang

2. 项目图标

![logo](./XLang3-small.png)

3. 项目主页：[https://nop-platform.github.io/projects/nop-entropy/docs/dev-guide/xlang/](https://nop-platform.github.io/projects/nop-entropy/docs/dev-guide/xlang/)

4. 项目仓库：[https://github.com/entropy-cloud/nop-entropy](https://github.com/entropy-cloud/nop-entropy)

5. 项目简介：

```xml
<c:log info="hello world" /> 
```

XLang 是一款混合采用XML标签语法和JavaScript语法的脚本语言，设计者为canonical。XLang语言是Nop低代码平台的底层支撑技术之一，它是世界上第一个内置了可逆计算理论差量合并算子支持的程序语言。

Nop平台采用面向语言编程范式（Language Oriented Programming），在应用开发时并不是直接使用通用程序语言（如Java、C#）来开发，而是先定义一个领域特定语言（DSL），然后再应用DSL来表达业务。为了快速开发和扩展DSL语言，我们需要一种能够定义DSL语法结构的元模型语言，同时还需要一系列的机制快速实现DSL的解释器、语法制导翻译等。XLang语言包含了XDef元模型定义语言，Xpl模板语言，XScript表达式语言和XTransform结构转换语言等子语言，它们共同构成一个完整的DSL开发基础设施。通过增加简单的XDef元模型定义，我们就可以自动得到对应的DSL解析器、验证器、IDE插件、调试工具等，并自动为DSL领域语言增加模块分解、差量定制、元编程等通用语言特性。

## 通过XDef元模型定义新的DSL

```xml
<!--
状态机模型DSL
@initial 初始状态的id
@stateProp 实体上的状态属性名
-->
<state-machine initial="!var-name" stateProp="!string" ignoreUnknownTransition="!boolean=false"
               xdef:name="StateMachineModel" xdef:bean-package="io.nop.fsm.model"
               x:schema="/nop/schema/xdef.xdef" xmlns:x="/nop/schema/xdsl.xdef" xmlns:xdef="/nop/schema/xdef.xdef"
>
    ...
    <state id="!var-name" xdef:unique-attr="id" xdef:ref="StateModel"/>

    <!-- 进入状态时触发的监听函数 -->
    <on-entry xdef:value="xpl"/>

    <!-- 离开状态时触发的监听函数 -->
    <on-exit xdef:value="xpl"/>

    <!--
    状态迁移出现异常时触发的监听函数。如果返回true，则认为异常已经被处理，不对外抛出异常
    -->
    <handle-error xdef:value="xpl-fn:(err)=>boolean"/>
</state-machine>
```

## XML和表达式语法的相互嵌入

XLang没有采用jsx语法实现类XML语法，而是沿用XML语法，扩展JavaScript中的Template表达式语法。

```javascript
let resut = xpl `<my:MyTag a='1' />`
const y = result + 3;
```

等价于

```xml
<my:MyTag a='1' xpl:return="result" />
<c:script>
  const y = result + 3;
</c:script>
```

XLang的整体结构严格符合XML格式要求，因此它作为模板语言来生成XML时满足Lisp语言所首创的同像性（Homoiconicity），因此特别适合元编程和宏函数的实现。

XLang修改了JavaScript中的Template表达式语法的解析格式，将反引号字符之间的内容识别为一个在编译期待解析的字符串，而不是一个Expression列表。这使得XLang可以利用这个语法形式扩展支持更多的DSL格式，比如引入类似C#的LinQ语法

```javascript
const result = linq `select sum(amount) from myList where status > ${status}`
```

实现这种类似linq语法的解析器非常简单，只需要在Java中定义一个静态函数，标记为`@Macro`

```javascript
    @Description("编译并执行xpl语言片段，outputMode=none")
    @Macro
    public static Expression xpl(@Name("scope") IXLangCompileScope scope, 
        @Name("expr") CallExpression expr) {
        return TemplateMacroImpls.xpl(scope, expr);
    }
```

## 宏函数和编译期执行

XLang支持编译期元编程，允许在编译期执行图灵完备的代码，并动态生成新的待编译的语法结构。

```xml
 <!--在编译期解析标签体得到ValidatorModel, 保存为编译期的变量validatorModel-->
<c:script><![CDATA[
import io.nop.biz.lib.BizValidatorHelper;

let validatorModel = BizValidatorHelper.parseValidator(slot_default);
// 得到<c:script>对应的抽象语法树
let ast = xpl `
   <c:ast>
    <c:script>
      import io.nop.biz.lib.BizValidatorHelper;
      if(obj == '$scope') obj = $scope;
      BizValidatorHelper.runValidatorModel(validatorModel,obj,svcCtx);
    </c:script>
   </c:ast>
`
// 将抽象语法树中的标识名称替换为编译期解析得到的模型对象。这样在运行期就不需要动态加载模型并解析
return ast.replaceIdentifier("validatorModel",validatorModel);
]]></c:script>
```

## XDSL的差量生成与合并机制

Nop平台中所有的DSL都支持x-extends差量合并机制，通过它实现了可逆计算理论所要求的计算模式

> App = Delta x-extends Generator<DSL>

具体来说，所有的DSL都支持`x:gen-extends`和 `x:post-extends`
配置段，它们是编译期执行的Generator，利用XPL模板语言来动态生成模型节点，允许一次性生成多个节点，然后依次进行合并，具体合并顺序定义如下：

```
<model x:extends="A,B">
    <x:gen-extends>
        <C/>
        <D/>
    </x:gen-extends>

    <x:post-extends>
        <E/>
        <F/>
    </x:post-extends>
</model>
```

合并结果为

```
F x-extends E x-extends model x-extends D x-extends C x-extends B x-extends A
```

当前模型会覆盖`x:gen-extends`和`x:extends`的结果，而`x:post-extends`会覆盖当前模型。

借助于`x:extends`和`x:gen-extends`
我们可以有效的实现DSL的分解和组合。具体介绍参见 [XDSL：通用的领域特定语言设计](https://zhuanlan.zhihu.com/p/612512300)

## 可扩展语法

 类似于Lisp语言，可以通过宏函数和标签函数等机制扩展XLang的语法。可以通过`<c:lib>`来引入新的语法节点，然后在该节点内部再通过宏函数等机制实现结构转换。

```xml
<c:lib from="/nop/core/xlib/biz.xlib" />
<biz:Validator fatalSeverity="100"
               obj="${entity}">

    <check id="checkTransferCode" errorCode="test.not-transfer-code"
           errorDescription="扫入的码不是流转码">
        <eq name="entity.flowMode" value="1"/>
    </check>
</biz:Validator>
```

  `<biz:Validator>`引入一个验证用的DSL，Validator标签在编译的时候会利用宏函数机制解析节点内容，将它翻译为XLang的Expression来运行。  

6. 项目分类：免费、开源（AGPLv3）、通用、接受社区贡献  
   语言类别：通用编程语言、命令式和函数式混合语言、可扩展语言、解释型脚本语言  
   工具类别：脚本语言、解释器、代码生成  
   应用领域：通用、行业应用  

7. 联系方式：  
   canonical_entropy@163.com（电子邮箱）  
