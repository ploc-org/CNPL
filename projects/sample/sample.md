
1. 项目名称：sample

2. 项目图标：

![](./logo.png)

3. 项目主页：[https://gitcode.com/ploc-org/CNPL](https://gitcode.com/ploc-org/CNPL)

4. 项目仓库：[https://gitcode.com/ploc-org/CNPL](https://gitcode.com/ploc-org/CNPL)

5. 项目分类：免费、开源（MIT）、通用、接受社区贡献  
语言类别：一般编程语言、命令式语言  
工具类别：一般编译工具、代码生成  
应用领域：通用、行业应用  

6. 项目简介。文字800字以内；图片不多于4张，图片尺寸：1920×1080像素 或 20×11.25英寸（96ppi），图片格式：JPG（首选）、PNG。

项目简介模板，案例截图：

![](./test.png)

7. 联系方式（至少填一项，括号内注明类型）：xxxyyyzzz（微信）、+86-13333333333（电话）、xyz@xyz.xyz（电子邮箱）

> 注：以上文档使用5号字体，A4幅面排版时，总篇幅不超过4页，最终版式参考"sample.doc"。