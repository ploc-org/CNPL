
1. 项目名称：HVML

2. 项目图标：

![](./hvml-logo.png)

3. 项目主页：[https://hvml.fmsoft.cn/](https://hvml.fmsoft.cn/)

4. 项目仓库：[https://github.com/HVML](https://github.com/HVML)

5. 项目简介：

HVML是Hybrid Virtual Markup Language（混合虚拟标记语言）的缩写。它通过标记语言的方式来组织呈现代码。Virtual表示通过赋予标记语言编程能力，使得该语言成为一种抽象化后的虚拟标记语言。Hybrid表示混合，它能够通过胶水的方式组织各种不同的语言或者程序。

HVML 的基本设计目标是，在已有的以C/C++，Python等编程语言构造的原生运行时环境中，利用现代 Web 前端技术(HTML/SVG、DOM、CSS 等)快速开发图形用户界面程序，而不需要借助额外的浏览器或者JavaScript引擎。

描述性是HVML的特点。描述性的语言不但能够方便开发者理解和撰写代码，也适合AI程序进行学习和代码生成。
![描述性语法](./hvml-descriptive.png)
HVML可以非常方便的与其他程序进行数据交互，比如和高精度计算程序`bc`交互实现图形界面版的高精度计算器：
![高精度计算器](./hvml-calculator-bc.png)
以及可以内嵌python代码，与python程序进行数据交互，处理和显示：
![内嵌Python代码](./hvml-embed-python-animated-3d-random-walk.png)

作为一个标记语言，标记符号的引入使得代码字符数量比其他语言要多，但是好处是对代码的组织会显得更加清晰。这是因为它的另一个设计目标就是借助自动化图形化低代码的开发工具来进行程序开发，同时清晰的组织也方便接入AI应用。

6. 项目分类：免费、开源（多种许可证方式）、通用、接受社区贡献
语言类别：运行时环境
工具类别：一般编译工具
应用领域：通用

7. 联系方式：yongkang.l@outlook.com
