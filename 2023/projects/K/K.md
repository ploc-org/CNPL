1. 项目名称：K lang

2. 项目图标：暂无

3. 项目主页：暂无

4. 项目仓库：[https://github.com/kulics-works/k](https://github.com/kulics-works/k)

5. 项目简介:

K 语言是面向应用领域的开源编程语言。具有静态类型、内存托管、多范式的特点。

现阶段 K 语言的主要目标是探索类型系统和语法设计，还不具备任何商用能力，也不承诺任何稳定性。

目前 K 语言尝试了几个比较有意思的设计：

- 前置括号的泛型语法
- 基于分号和块区分的表达式结构语法
- 可参数化的可变类型限定符

代码案例：

```
## case1
type (T1, T2)Pair(left: T1, right: T2);

let main() = {
    lef a1: (Int, Int)Pair = (Int, Int)Pair(1, 2);
    ## a1.left: Int, a1.right: Int
    lef a2: (Bool, Bool)Pair = (Bool, Bool)Pair(true, false);
    ## a2.left: Bool, a2.right: Bool
    lef a3: (Int, String)Pair = (Int, String)Pair(1, "a");
    ## a3.left: Int, a3.right: String
}

## case2
let main() = {
    if true || f() do {
        ...
    }
    0
}

## case3
type mut Point(x: Int, y: Int);

let main() = {
    let a: mut Point = mut Point(64, 128); 
    let b: Point = a; ## ok
    printLine(a.x); ## 64
    printLine(b.x); ## 64
    a.x = 128;
    printLine(a.x); ## 128
    printLine(b.x); ## 128
    b.x = 256; ## error
}
```

6. 项目分类：免费、开源（MIT）、通用、接受社区贡献  
语言类别：一般编程语言  
工具类别：一般编译工具  
应用领域：通用 


7. 联系方式：kulics@outlook.com